package com.example.syathish.screenoverlay;

import android.app.Service;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

public class OverlayService extends Service {
    WindowManager windowManager;
    LinearLayout mView;
    public OverlayService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH | WindowManager.LayoutParams.FLAG_SECURE,
                PixelFormat.TRANSPARENT);
        params.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
        params.gravity = Gravity.CENTER;
        mView=new LinearLayout(this);
        View button=new View(this);
        mView.addView(button);
        windowManager= (WindowManager) getSystemService(WINDOW_SERVICE);
        windowManager.addView(mView,params);
        Toast.makeText(this, "Have set this dude", Toast.LENGTH_SHORT).show();
    }


    @Override
    public IBinder onBind(Intent intent) {
       return null;
    }
    @Override
    public void onDestroy() {
        windowManager.removeView(mView);
        super.onDestroy();
    }
}
